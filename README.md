[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
---

Sample EPUB files for testing/development

Forked from [EPUB samples](https://github.com/bmaupin/epub-samples.git)

- basic-v3plus2
  - Based off minimal-v3plus2 with a few additional basic features:
    - Author
    - CSS stylesheets
    - Custom font
    - An extra section
    - Cover page
- minimal-v2
  - EPUB 2.0 sample with minimal features
- minimal-v3
  - EPUB 3.0 sample with minimal features
- minimal-v3plus2
  - EPUB 3.0 sample with minimal features and an additional EPUB 2.0 table of contents for maximum backwards compatibility

To build the EPUB files, run the `pack-epubs.sh` script.

All samples have been validated with the [EPUB Validator](http://validator.idpf.org/).

These samples are for testing/development and are intentionally minimal. For samples with more features, see https://github.com/IDPF/epub3-samples
